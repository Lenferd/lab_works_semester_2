#pragma once
#ifndef _VECTOR_H_
#define _VECTOR_H_

#include <iostream>
using namespace std;

typedef double TYPE;
class CVector
{
public:
	CVector();
	CVector(unsigned int);
	CVector(const CVector&);
	~CVector();
	
	void Set();
	void RandSet(int);

	TYPE dotpro(const CVector&, const CVector&); 
	CVector operator+(const CVector&);	
	CVector operator-(const CVector&);
	CVector& operator+=(const CVector&);
	CVector& operator-=(const CVector&);

	CVector operator*(const TYPE);
	CVector& operator*=(const TYPE);

	CVector& operator=(const CVector&);
	bool operator==(const CVector&) const;
	bool operator!=(const CVector&) const;
	TYPE& operator[](unsigned int);
	friend ostream& operator<<(ostream&, const CVector&);

private:
	void random(int);
	TYPE *data;
	unsigned int size;
};
#endif 
