#include "Vector.h"
#include <iostream>
#include <ctime>
using namespace  std;

/*
/	TO-DO
/	1) ���������� operator== � ����������� try{} catch{}. ���� �� ��������, ��� ��������� �����������.
*/

CVector::CVector()
{
	data = new TYPE[1];
	size = 1;
	data[0] = 0;
}

//����������� � �������� ��������. 
CVector:: CVector(const unsigned int _size)
{
	if (_size!=0)
	{
		data = new TYPE[_size];	
		size = _size;
		for (int i = 0; i < size; i++)
			data[i] = 0;
	} 
	else 
	{
		data = new TYPE[1];
		size = 1;
		data[0] = 0;
	}
}

CVector:: CVector(const CVector&vect)
{
	this->size=vect.size;
	this->data=new TYPE[size];
	for(int i=0; i<size; i++)
		data[i]=vect.data[i];
}

CVector::~CVector(void)
{
	delete[] data; 
}


//����� ����������� ������� � ��� ����������
void CVector::Set()
{
	cout << "Enter size of vector"<<endl;
	cin >> this->size;
	this->data = new TYPE[size];
	cout << "Enter vector coordinates"<<endl;
	for (int i = 0; i<size; i++)
		cin >> data[i];
}

//range - �������� ��� ������� 
void CVector::RandSet(int _range)
{
	cout << "Enter size of vector"<<endl;
	cin >> this->size;
	this->data = new TYPE[size];
	this->random(_range);
}

//dot product = ��������� ������������
TYPE CVector::dotpro(const CVector&vect1, const CVector&vect2)
{
	if (vect1.size == vect2.size)
	{
		TYPE result = 0;
		for (int i = 0; i < vect1.size; i++)
			result += vect1.data[i] * vect2.data[i];
		return result;
	}
	else
	{
		TYPE result = 0;
		cout << "Operation is not possible, sizes do not match" << endl;
		return result;
	}
}

CVector CVector::operator+(const CVector&vect)
{
	if (size == vect.size)
	{
		CVector result(vect.size);
		for (int i = 0; i < size; i++)
			result.data[i] = data[i] + vect.data[i];
		return result;
	}
	else
	{
		cout << "Operation is not possible, sizes do not match" << endl;
		return CVector();
	}
}

CVector CVector::operator-(const CVector&vect)
{
	if (size == vect.size)
	{
		CVector result(vect.size);
		for (int i = 0; i < size; i++)
				result.data[i] = data[i] - vect.data[i];
		return result;
	}
	else
	{
		cout << "Operation is not possible, sizes do not match" << endl;
		return CVector();
	}
}

CVector& CVector::operator+=(const CVector&vect)
{
	if (size == vect.size)
	{
		for (int i = 0; i < size; i++)
			this->data[i] += vect.data[i];
		return *this;
	}
	else
	{
		cout << "Operation is not possible, sizes do not match" << endl;
		return CVector();
	}
}

CVector& CVector::operator-=(const CVector&vect)
{
	if (size == vect.size)
	{
		for (int i = 0; i < size; i++)
			this->data[i] -= vect.data[i];
		return *this;
	}
	else
	{
		cout << "Operation is not possible, sizes do not match" << endl;
		return CVector();
	}
}

CVector CVector::operator*(const TYPE value)
{
	CVector result(this->size);
	for (int i = 0; i < this->size; i++)
		result.data[i] =this->data[i] * value;
	return result;
}

CVector& CVector::operator*=(const TYPE value)
{
	for (int i = 0; i < this->size; i++)
		this->data[i] *= value;
	return *this;
}

CVector& CVector::operator=(const CVector&vect)
{
	//���������, �� ����� �� ����� (���� � ��� �� ������), ����� ����� �� ������� ��� ��������. 
	//���� ����� �����, �� ������ ���������� ���� �� ������. (������ vect1=vect1).

	if (this!=&vect)			
	{
		delete[] this->data;
		if (vect.data!=NULL)
		{
			this->size=vect.size;
			this->data=new TYPE[size];
			for(int i=0; i<size; i++)
				this->data[i]=vect.data[i];
		}
		else 
		{
			this->data=0;
			this->size=0;
		}
	}
	return *this;
}

bool CVector::operator==(const CVector&vect) const 
{
	if (this->size==vect.size)
	{
		for (int i = 0; i<size; i++)
			if (this->data[i]!=vect.size)
				return false;
		return true;
	}
	else 
	{
		return false;
	}
}

bool CVector::operator!=(const CVector&vect) const 
{
	if (this->size==vect.size)
	{
		for (int i; i<size; i++)
			if (this->data[i]!=vect.size)
				return true;
		return false;
	}
	else 
	{
		return true;
	}
}

TYPE& CVector::operator[](unsigned int i)
{
	if (size && i >= 0 && i < size)
		return data[i];
	else
	{
		cout << "Access violation" << endl;
		exit(1);
	}
}

ostream& operator<<(ostream&out, const CVector&vect)
{
	out << '[';
	for (int i = 0; i < vect.size; i++)
	{
		out << vect.data[i];
		if (i != vect.size - 1)
			out << ';';
	}
	out << ']';
	return out;
}

void CVector::random(int _range)
{
	srand(time(0));
	for (int i=0; i<this->size; i++)
		data[i]=rand()%_range;
}