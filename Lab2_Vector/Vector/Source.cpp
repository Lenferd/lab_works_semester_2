#include <iostream>
#include "Vector.h"
using namespace std;

int main()
{
	int range, digit;
	cout << "Creation of the first vector" << endl;
	CVector vect1;
	vect1.Set();
	cout << endl;
	cout << "Creation of the second vector (random)" << endl;
	CVector vect2;
	cout << "Enter the range of random" << endl;
	cin >> range;
	vect2.RandSet(range);
	cout << endl;
	cout << "First vector" << endl << vect1 << endl;
	cout << "Second vector" << endl << vect2 << endl;
	cout << endl;
	if (vect1 == vect2)
		cout << "vect1 = vect2" << endl;
	else cout << "vect1 != vect2" << endl;
	cout << "vect1+vect2:\t" << vect1 + vect2 << endl;
	cout << "vect1-vect2:\t" << vect1 - vect2 << endl;
	cout << "dot product:\t" << vect1.dotpro(vect1, vect2) << endl;
	cout << endl;
	cout << "Enter digit" << endl; cin >> digit;
	cout << "vect1 * " << digit << ":\t" << vect1 * digit << endl;
	cout << "vect2 * " << digit << ":\t" << vect2 * digit << endl;
	return 0;
}