#include "fraction.h"
#include <iostream>
#include <stdio.h>

using namespace std;
/*
/	������
/	int numerator - ���������
/	int denominator - �����������
/
/	TO-DO
*/


Fraction::Fraction(void)
{
	numerator = 0;
	denominator = 1;
}


void Fraction::SetFrac()
{
	int a, b;
	cout << "Enter fraction, for example: \"1/2\"" << endl;
	scanf("%d%*c%d", &a, &b);
	while (b == 0)
	{
		cout << "Error! Denominator cannot be 0" << endl;
		cout << "Please, try again" << endl;
		scanf("%d%*c%d", &a, &b);
	}
	numerator = a;
	denominator = b;
}

Fraction Fraction::operator+(const Fraction&fract2) const
{
	Fraction result;
	if (this->denominator == fract2.denominator)
	{
		result.numerator = this->numerator + fract2.numerator;
		result.denominator = this->denominator;
	}
	else
	{
		result.numerator = this->numerator*fract2.denominator + this->denominator*fract2.numerator;
		result.denominator = this->denominator*fract2.denominator;
	}

	result.Downgrade();
	return result;
}

Fraction Fraction::operator-(const Fraction&fract2) const
{
	Fraction result;
	if (this->denominator == fract2.denominator)
	{
		result.numerator = this->numerator - fract2.numerator;
		result.denominator = this->denominator;
	}
	else
	{
		result.numerator = this->numerator*fract2.denominator - this->denominator*fract2.numerator;
		result.denominator = this->denominator*fract2.denominator;
	}

	result.Downgrade();
	return result;
}

Fraction Fraction::operator*(const Fraction&fract) const
{
	Fraction result;

	result.numerator = this->numerator*fract.numerator;
	result.denominator = this->denominator*fract.denominator;
	result.Downgrade();
	return result;
}

Fraction Fraction::operator/(const Fraction&fract) const
{
	Fraction result;

	result.numerator = this->numerator*fract.denominator;
	result.denominator = this->denominator*fract.numerator;
	result.Downgrade();
	return result;
}

Fraction Fraction::operator*(const int value) const
{
	Fraction result;

	result.numerator = this->numerator*value;
	result.denominator = this->denominator;
	result.Downgrade();
	return result;
}

Fraction Fraction::operator/(const int value) const
{
	Fraction result;

	result.numerator = this->numerator;
	result.denominator = this->denominator*value;
	result.Downgrade();
	return result;
}

Fraction Fraction::operator+(const int value) const 
{
	Fraction result;

	result.denominator = this->denominator;
	result.numerator = this->numerator + (value*this->denominator);
	result.Downgrade();
	return result;
}

Fraction Fraction::operator-(const int value) const 
{
	Fraction result;

	result.denominator = this->denominator;
	result.numerator = this->numerator - (value*this->denominator);
	result.Downgrade();
	return result;
}
bool Fraction::operator==(const Fraction&fract) const
{
	if ((this->numerator == fract.numerator) && (this->denominator == fract.denominator))
		return true;
	else return false;
}

bool Fraction::operator!=(const Fraction&fract) const
{
	if ((this->numerator == fract.numerator) && (this->denominator == fract.denominator))
		return false;
	else return true;
}

Fraction& Fraction::operator=(const Fraction&fract)
{
	this->numerator = fract.numerator;
	this->denominator = fract.denominator;
	return *this;
}

Fraction& Fraction::operator+=(const Fraction&fract2)
{
	if (this->denominator == fract2.denominator)
		this->numerator = this->numerator + fract2.numerator;
	else
	{
		this->numerator = this->numerator*fract2.denominator + this->denominator*fract2.numerator;
		this->denominator = this->denominator*fract2.denominator;
	}
	this->Downgrade();
	return *this;
}

Fraction& Fraction::operator-=(const Fraction&fract2)
{
	if (this->denominator == fract2.denominator)
		this->numerator = this->numerator - fract2.numerator;
	else
	{
		this->numerator = this->numerator*fract2.denominator - this->denominator*fract2.numerator;
		this->denominator = this->denominator*fract2.denominator;
	}
	this->Downgrade();
	return *this;
}

Fraction& Fraction::operator*=(const Fraction&fract2)
{
	this->numerator = this->numerator * fract2.numerator;
	this->denominator = this->denominator * fract2.denominator;
	this->Downgrade();
	return *this;
}

Fraction& Fraction::operator/=(const Fraction&fract2)
{
	this->numerator = this->numerator * fract2.denominator;
	this->denominator = this->denominator * fract2.numerator;
	this->Downgrade();
	return *this;
}

double Fraction::decimal()
{
	double result, a, b;
	a = this->numerator; b = this->denominator;
	result = a / b;
	return result;
}

ostream& operator<<(ostream&out, const Fraction&fract)
{
	if (fract.denominator == 1)
	{
		out << fract.numerator;
	}
	else if (fract.denominator == -1)
	{
		out << -fract.numerator;
	}
	else
	{
		out << fract.numerator << '/' << fract.denominator;
	}
	return out;
}

void Fraction::Downgrade(void)	//���� �����, ��������� ��������� � ����������� (�������� �������� 5/10 � 1/2)
{
	if ((this->numerator >= 0) && (this->denominator < 0))		//�������� ����� ���� a/-b � -a/b
	{
		this->numerator *= -1; 
		this->denominator *= -1;
	}
	if ((this->numerator < 0) && (this->denominator < 0))		//�������� ����� ���� -a/-b � a/b
	{
		this->numerator *= -1; 
		this->denominator *= -1;
	}

	if (denominator != 0 && numerator != 0)
	{
		int common_div;			//����. ���. ��������
		int a = abs (denominator);	//abs - ������, ����������� ������ �����
		int b = abs (numerator);
		while (a && b)		//a!=0 � b!=0
		{
			if (a>b)
				a %= b;
			else 
				b %= a;
		}
		common_div=a+b;
		denominator/=common_div;
		numerator/=common_div;
	}
	else
	{
		this->numerator = 0;
		this->denominator = 1;
	}
}