#pragma once
#include <iostream>;

using namespace std;

class Fraction
{
public:
	Fraction(void);
	void SetFrac();
	Fraction operator+(const Fraction&) const;
	Fraction operator-(const Fraction&) const;
	Fraction operator*(const Fraction&) const;
	Fraction operator/(const Fraction&) const;
	Fraction operator*(const int) const;
	Fraction operator/(const int) const;
	Fraction operator+(const int) const;
	Fraction operator-(const int) const;
	bool operator==(const Fraction&) const;
	bool operator!=(const Fraction&) const;
	Fraction& operator=(const Fraction&);
	Fraction& operator+=(const Fraction&);
	Fraction& operator-=(const Fraction&);
	Fraction& operator*=(const Fraction&);
	Fraction& operator/=(const Fraction&);
	double decimal();
	friend ostream& operator<<(ostream&, const Fraction&);


private:
	int numerator;
	int denominator;
	void Downgrade();
};