#include <iostream>
#include "fraction.h"

using namespace std;

int main()
{
	Fraction first, second;
	int question;

	do
	{
		first.SetFrac();
		second.SetFrac();
		cout << "\nExamples:" << endl;
		if (first == second)
			cout << "first == second" << endl;
		else
			cout << "first != second" << endl;
		cout << "Fract1+Fract2:\t" << first + second << endl;
		cout << "Fract1-Fract2:\t" << first - second << endl;
		cout << "Fract1*Fract2:\t" << first * second << endl;
		cout << "Fract1/Fract2:\t" << first / second << endl;
		cout << "decimal first = " << first.decimal() << endl << "decimal second = " << second.decimal() << endl;
		cout << "\nAgain? (1 or 0)" << endl;
		cin >> question;
	} while (question == 1);

	return 0;
}